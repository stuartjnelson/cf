sch<?php
/***
* 		Template Name: Contact
***/

get_header(); ?>

<section class="section--hero section--contactPage">

<div id="mapCanvas"></div>

	<article class="contactPage__details">

        <div class="contactPage__column contactPage__school2">
            <h3 class="contactPage__header header__school2">Monkton Farleigh Base</h3>

            <ul class="contactItemContainer">
                <?php if ( get_field('phone_number2') ) : ?>
                    <li class="contactItem contactIcon--phone">
                        <?php echo get_field('phone_number2'); ?>
                    </li>
                <?php endif; ?>   

                <?php if ( get_field('email_address2') ) : ?>
                    <li class="contactItem contactIcon--email">
                        <?php echo get_field('email_address2'); ?>
                    </li>
                <?php endif; ?>

                <?php if ( get_field('address2') ) : ?>
                    <li class="contactItem contactIcon--address">
                        <?php echo get_field('address2'); ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div>

		<div class="contactPage__column contactPage__school1">
			<h3 class="contactPage__header header__school1">Atworth Base</h3>
            <ul class="contactItemContainer">
            	<?php if ( get_field('phone_number1') ) : ?>
                    <li class="contactItem contactIcon--phone">
                        <?php echo get_field('phone_number1'); ?>
                    </li>
                <?php endif; ?>   

                <?php if ( get_field('email_address1') ) : ?>
                    <li class="contactItem contactIcon--email">
                        <?php echo get_field('email_address1'); ?>
                    </li>
                <?php endif; ?>

                <?php if ( get_field('address1') ) : ?>
                    <li class="contactItem contactIcon--address">
                        <?php echo get_field('address1'); ?>
                    </li>
                <?php endif; ?>    
            </ul> 
		</div>
    </article>    
		
	<div class="contactPage__contactFormContainer">
        <h2 class="section__title">
            Get in touch!
        </h2>

        <?php echo do_shortcode('[contact-form-7 id="60" title="Contact form 1"]');?>
    </div>
		
	
</section>
<?php get_footer(); ?> 	