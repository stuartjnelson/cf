<?php get_header(); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

		<article class="customPostType">
			<h1 class="customPostType__title">
				<?php the_title(); ?>
			</h1>

			<?php if (get_field('ps_image')): ?>

				<?php 
					$single_image = get_field('ps_image');
					
					//if( !empty($single_image) ): 
					
					$url = $single_image['url'];
					$title = $single_image['title'];
					$alt = $single_image['alt'];
					
					$size = 'hero'; 
					$thumb = $single_image['sizes'][ $size ];
					$width = $single_image['sizes'][ $size . '-width' ];
					$height = $single_image['sizes'][ $size . '-height' ];
	
					$new_size 	= '-1440x350.jpg';
					$new_url	= $url;
					$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
				?>
				<img 
					src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
					data-src="	<?php echo $new_image; ?>"
					alt="		<?php echo $alt; ?>" 
					title="		<?php echo $title; ?>" 
					width="		<?php echo $width; ?>" 
					height="	<?php echo $height; ?>"
				>	
			<?php endif ?>	
	
			<div class="customPostType__copy">
				 <?php the_content(); ?>
			</div>
	
		</article>

	<?php endwhile; ?> <!-- /main loop -->

<?php get_footer(); ?>		