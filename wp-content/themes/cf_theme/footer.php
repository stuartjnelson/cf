    </main>

    <footer class="footer">  

        <div class="footer__innerContainer">
            <div class="footer__footerTitle">
                <h3 class="footer__footerTitle--title">
                    Zones
                </h3>
            </div> 
            <nav class="zonesMenu">
                <?php if ( get_field('governor_zone', 'options') ) : ?>
                  <?php $gov_zone = get_field('governor_zone', 'options'); ?>
                <?php endif; ?>

                <?php if ( get_field('pta_zone', 'options') ) : ?>
                  <?php $pta_zone = get_field('pta_zone', 'options'); ?>
                <?php endif; ?>
    
                <?php if ( get_field('policies_zone', 'options') ) : ?>
                  <?php $pol_zone = get_field('policies_zone', 'options'); ?>
                <?php endif; ?>
    
                <?php if ( get_field('general_info_zone', 'options') ) : ?>
                  <?php //$gen_zone = get_field('general_info_zone', 'options'); ?>
                <?php endif; ?>
    
                <?php if ( get_field('love_learning_zone', 'options') ) : ?>
                  <?php $lve_zone = get_field('love_learning_zone', 'options'); ?>
                <?php endif; ?>

                <?php if ( get_field('parent_zone', 'options') ) : ?>
                  <?php $parent_zone = get_field('parent_zone', 'options'); ?>
                <?php endif; ?>

                <ul class="zonesMenu__list footerMenu">
                    <li class="zonesMenu__item">
                        <a href="<?php echo $gov_zone; ?>" class="button button--small button--zones buttonZones--gov">
                            Governor Zone
                        </a>
                    </li>
                    <li class="zonesMenu__item">
                        <a href="<?php echo $pta_zone; ?>" class="button button--small button--zones buttonZones--pta">
                            PTA Zone
                        </a>
                    </li>
                    <li class="zonesMenu__item">
                        <a href="<?php echo $pol_zone; ?>" class="button button--small button--zones buttonZones--pol">
                            Policies Zone
                        </a>
                    </li>
                    <!-- <li class="zonesMenu__item">
                        <a href="<?php //echo $gen_zone; ?>" class="button button--small button--zones buttonZones--gen">
                            General Info Zone
                        </a>
                    </li> -->
                    <li class="zonesMenu__item">
                        <a href="<?php echo $lve_zone; ?>" class="button button--small button--zones buttonZones--lve">
                            Love Learning Zone
                        </a>
                    </li>
                    <li class="zonesMenu__item">
                        <a href="<?php echo $parent_zone; ?>" class="button button--small button--zones buttonZones--parent">
                            Parent Zone
                        </a>
                    </li>
                </ul>
            </nav>
        </div> 

        <div class="footer__innerContainer">
            <div class="footer__footerTitle">
                <h3 class="footer__footerTitle--title">
                    Latest Events
                </h3>
            </div>   
            <?php if ( is_active_sidebar( 'calendar_widget' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'calendar_widget' ); ?>
                </div>
            <?php endif; ?>       
        </div> 

        <div class="footer__innerContainer">
            <div class="footer__footerTitle">
                <h3 class="footer__footerTitle--title">
                    Useful Links
                </h3>
                <ul class="footerMenu usefulLink">
                    <?php if ( have_rows('useful_page_links', 'options') ) : ?>
                    
                        <?php while( have_rows('useful_page_links', 'options') ) : the_row(); ?>
                            <?php if (get_sub_field('internal_or_external_link') == 'internal'): ?>
                                <?php 
                                $post_objects = get_sub_field('useful_page_link', 'options');
                                if( $post_objects ): ?>
    
                                <li>
                                    <a href="<?php echo get_permalink($post_objects); ?>">
                                        <?php echo get_the_title($post_objects); ?>
                                    </a>
                                </li>    
                                <?php  endif; ?>
                            <?php else: ?>
                                <li>
                                    <a href="<?php the_sub_field('external_link'); ?>">
                                        <?php the_sub_field('external_link_text'); ?>
                                    </a>
                                </li>    
                            <?php  endif; ?>    
                      <?php endwhile; ?>
                    
                    <?php endif; ?>
                </ul>
            </div>          
        </div>               

        <div class="footer__innerContainer">
            <?php 
                if (is_home() || is_page(4)) {
                    $postType       = 'school_news';
                    $Feedtitle      = 'Latest Announcements';
                } else {
                    $postType   = 'post';
                    $Feedtitle      = "Churchfield's Blog";
                }
            ?>

            <div class="footer__footerTitle">
                <h3 class="footer__footerTitle--title">
                    <?php echo $Feedtitle; ?>
                </h3>
            </div>    
            <?php     
                $args = array( 
                    'post_type'         => $postType,
                    'posts_per_page'    => 2,
                    //'order'             => 'DEC'
                );
                $the_query = new WP_Query( $args );
            ?>
            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <article class="blogPost blogPost--footer">
                    <div class="blogPostFooter__leftColumn">
                    <?php if (get_field('single_image')): ?>
                        <a href="<?php the_permalink();?>">
                            <?php 
                                $image = get_field('single_image');
                                 
                                
                                $url = $image['url'];
                                $title = $image['title'];
                                $alt = $image['alt'];
                                
                                $size = 'blogFooterThumbail'; 
                                $thumb = $image['sizes'][ $size ];
                                $width = $image['sizes'][ $size . '-width' ];
                                $height = $image['sizes'][ $size . '-height' ];
                                $new_size   = '-100x70.jpg';
                                $new_url    = $url;
                                $new_image  = preg_replace('/.jpg/', $new_size, $new_url);
                            
                            ?>
                            <img 
                                class="blogPost__image--footer" 
                                src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                data-src="<?php echo $new_image; ?>"
                                alt="<?php echo $alt; ?>" 
                                title="<?php echo $title; ?>" 
                                width="<?php echo $width; ?>" 
                                height="<?php echo $height; ?>"
                            >
                        </a>
                    <?php endif ?>    
                    </div>    
            
                    <div class="blogPostFooter__rightColumn">
                        <h4 class="blogPost__title blogPost__title--footer">
                            <?php the_title(); ?>
                        </h4>
                        <a href="<?php the_permalink();?>" class="button button--small blogPost__link">Read more</a>
                    </div>                                
                </article>
            <?php endwhile; endif; wp_reset_postdata(); ?><!-- ending custom query loop --><!-- /ending custom query loop -->
        </div>     

        <div class="footer__legal">
            <div class="footer__legalColumn footer__legalColumn--left">
                <?php if ( get_field('t&c', 'options') ) : ?>
                  <a href="<?php echo get_field('t&c__link', 'options'); ?>" alt="<?php echo get_field('t&c', 'options'); ?>">
                    <?php the_field('t&c', 'options'); ?>
                  </a>
                <?php endif; ?>
            </div>
            <div class="footer__legalColumn footer__legalColumn--right">
                <?php if ( get_field('privacy', 'options') ) : ?>
                  <a href="<?php echo get_field('privacy__link', 'options'); ?>" alt="<?php echo get_field('privacy', 'options'); ?>">
                    <?php echo get_field('privacy', 'options'); ?>
                  </a>
                <?php endif; ?>
            </div>
        </div>

         <div class="footer__copyright">
             <div class="footer__copyrightInner">
                <a href="http://www.gumption.agency" alt="Gumption Agency">
                    Website run with Gumption
                </a>
             </div>
         </div>
    </footer>
        
    <?php wp_footer(); ?>

    <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "<?php echo get_template_directory_uri(); ?>/library/js/defer/defer.js";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
    </script>
  
    </body> 
</html>