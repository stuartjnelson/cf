<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>

	<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" media="all" type="text/css" />
</head>
<?php wp_head(); ?>
	
	<body class="<?php body_class( $class ); ?>">
		<div id="main-container">
			<div class="error-image"></div>
				<div class="error-container">
					<h1 class="error-title">Oops something went wrong there</h1>
				</div>
				<div class="error-container-two">	
					<a class="error-link" href="<?php $url = home_url(); echo $url; ?>">Homepage</a>
				</div>			
		</div>	<!-- End of #main-container -->	
	
	<?php wp_footer(); ?>
	</body>
