// as the page loads, call these scripts
//jQuery(document).ready(function($) {   

    var $document = $(document),
    $elementHeader     = $('.header'),
    classShadow     = 'headerShadow';
    heightHeader     = $('.header').height();
    
    $document.scroll(function() {
        $elementHeader.toggleClass(classShadow, $document.scrollTop() >= heightHeader);

        if ($('.subMenuContainer').hasClass('subMenu--active') || $('mobileMenu').hasClass('mobileMenu--active')) {
            $elementHeader.removeClass(classShadow);
        }
    });
    
	// add all your none-screen-size specific scripts here
    $(".mobileNavButton").click(function(){
        $(".mobileMenu").toggleClass("mobileMenu--active");
        $('.subMenuContainer').removeClass( 'subMenu--active' );
        $('.header').removeClass( 'headerShadow' );
        $(".mobileNavButton__icon").toggleClass('fa-bars'), 2500;
        $(".mobileNavButton__icon").toggleClass('fa-times'), 2500;
    });

    $(document).ready(function() {
        var blogHeight = $('.blogArticlesContainer').height();
        $('.sidebar').css('height', blogHeight);
    }); 

    $(document).ready(function() {
        var active              = 'subMenu--active';
        var parent              = '.menu-item-has-children';
        var secondLevelMenu     = '.subMenuContainer';
        var desktopClass        = 'desktop';
        
        function showingSubMenu($id, $name) {
            $('.menu-item-' + $id).click(function(){
                $(secondLevelMenu).removeClass( active );
                //$('.header').removeClass( '.hideHeaderShadow' );
                $('.subMenuContainer--' + $name).addClass( active );
                $('.header').removeClass( 'headerShadow' );
            });
        }
        
        showingSubMenu('266', 'about');
        showingSubMenu('894', 'meet');
        showingSubMenu('267', 'how');
        showingSubMenu('270', 'learn');
        showingSubMenu('269', 'news');


        $(window).on('resize', function () {
            var $window = $(window);

            //Desktop    
            if ( $window.width() > 874) {
                var t = null;
                
                $(".subMenuContainer").mouseleave(function() {
                    t = setTimeout(function() {
                        $('.subMenuContainer').removeClass('subMenu--active');
                    }, 1000);  
                }); 
                
                //Close mobile menu on desktop
                if ($('.mobileMenu').hasClass('mobileMenu--active')) {
                    $('.mobileMenu').removeClass('mobileMenu--active'); 
                    $(".mobileNavButton__icon").removeClass('fa-times'), 2500; 
                    $(".mobileNavButton__icon").addClass('fa-bars'), 2500;
                }

                function articleRow($number) {
                    //getting element in to variable
                    //var target  = $('.zoneContainer').attr('target'); //trying to use target only workons one 1
                    var target  = $number;
                
                    var one     = $('.articleRow--'+ target +'--one').height();
                    var two     = $('.articleRow--'+ target +'--two').height();
                    var three   = $('.articleRow--'+ target +'--three').height();
            
                    //compare values
                    var tallestItem = Math.max(one, two, three);
                
                    //Getting height
                    var itemHeight = tallestItem;
                
                    $('.articleRow--'+ target).css('height', itemHeight);
                } 

                articleRow(1);
                articleRow(2);
                articleRow(3);  
                articleRow(4);  
                articleRow(5);  
                articleRow(6);  
                articleRow(7);  
                articleRow(8);  
                articleRow(9);
                articleRow(10);  
            }      

            //Mobile
            else {
                $('.articleRow--1, .articleRow--2, .articleRow--3, .articleRow--4, .articleRow--5, .articleRow--6, .articleRow--7, .articleRow--8, .articleRow--9, .articleRow--10').css('height', 'inherit');
            }
        }).resize();

        //Stop scrolling to top when opening sub-menu

        $(document).ready(function() {
            $('.menu-item--parent').click(function(e) {
                return false;
                e.preventDefault();
            });

            //Close mobile menu button
            $('.mobileMenu__closeButton').click(function(){
                $(secondLevelMenu).removeClass( active );
            });
        });    

        //Close mobile menu when click off
        //$(document).mouseup(function (e){
        //    var container = $('.mobileMenu');
        //    var active = "mobileMenu--active"
        //
        //    if (!container.is(e.target) // if the target of the click isn't the container...
        //        && container.has(e.target).length === 0) // ... nor a descendant of the container
        //    {
        //        $(container).removeClass( active );
        //        $(".mobileNavButton__icon").removeClass('fa-times'), 2500;
        //        $(".mobileNavButton__icon").addClass('fa-bars'), 2500;
        //        $(".subMenuContainer").removeClass('subMenu--active'), 2500;
        //    }
        //});  
        
    }); 

    // 100% height
	$(document).ready(function(){function i(){windowHeight=$(window).innerHeight(),$(".fullsize").css("height",windowHeight)}i(),$(window).resize(function(){i()})});

$(document).ready(function(){
    var shareUrl      = window.location.href;

    //Social Sharing JS
    var SocialShares = {
        fb: {
          share: "http://www.facebook.com/sharer/sharer.php?u="
        },
        tw: {
          share: "https://twitter.com/intent/tweet?url="
        },
         gp: {
          share: "https://plus.google.com/share?url="
        }, 
    };
    
    $(function () {
        $('[data-social]').each(function () {
            var $this = $(this),
                social = $this.data('social'),
                oSocial;
            
            if (SocialShares.hasOwnProperty(social)) {
                oSocial = SocialShares[social];
              
                if (oSocial.share) {
                    $this.on("click", function () {
                        window.open(
                            oSocial.share + shareUrl,
                            '', 
                            'menubar=no,toolbar=no,resizable=yes' + 
                            ',scrollbars=yes' +
                            ',height=300,width=600'
                        );
                    });
                }
            }
        });
    });
}); 


	 
//}); /* end of as page load scripts */