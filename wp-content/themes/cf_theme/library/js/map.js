//      var map;
//      function initialize() {
//        var mapOptions = {
//          zoom: 15,
//          scrollwheel: false,
//          center: new google.maps.LatLng(51.391523, -2.202383),
//          mapTypeId: google.maps.MapTypeId.ROADMAP
//        };
//        map = new google.maps.Map(document.getElementById('mapCanvas'),
//            mapOptions);
//
//        google.maps.event.addDomListener(window, "resize", function() {
//   var center = map.getCenter();
//   google.maps.event.trigger(map, "resize");
//   map.setCenter(center); 
//});
//          var mapMarker = new google.maps.Marker({
//          position: new google.maps.LatLng(51.391523, -2.202383),
//          map: map,
//          title: 'Churchfield School'
//
//
//      });
//      }
//
//      google.maps.event.addDomListener(window, 'load', initialize);
    var responsive_viewport = $(window).width();
    
    var zoom = 13
    
    /* if is above or equal to 768px */
    if (responsive_viewport >= 1024) {
      var zoom = 14
}


var locations = [
      //['School One', 51.391523, -2.202383, 1],
      ['School One', 51.39209, -2.199883, 1],
      ['School Two', 51.387774, -2.282040, 2],
    ];
    var map = new google.maps.Map(document.getElementById('mapCanvas'), {
      
      zoom: zoom,
      scrollwheel: false,
      center: new google.maps.LatLng(((51.39209+51.387774)/2), ((-2.199883-2.282040)/2)),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
