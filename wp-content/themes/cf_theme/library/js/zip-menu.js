(function($) {

	var cutoffPoint = 765;
	
	/**
	* It's zippy!
	*
	* This is a tweaked version of the standard menu; it animates the dropdown
	**/
	$.fn.ZippMenu = function() {
		return this.each(function() {
			var $menu = $(this);
			var $tops = $menu.children('li.zippmenu-tab');
			var $topAs = $menu.children('li.zippmenu-tab').children('a');
			var $header = $('.header');
			var timeout = 0;
			
			// No IE6
			if ($.browser.msie && $.browser.version <= 6) {
				$tops.css('padding', 6);
				$tops.children('div').remove();
				return;
			}
			
			// If nothing in the popup div, nuke it
			$tops.children('div').each(function() {
				if ($.trim($(this).text()) == '') $(this).remove();
			});

			console.log($tops.children('div'));
			
			
			// Open a top-level item
			function open($li) {
				window.clearTimeout(timeout);

				if ($li.is('.hover')) return;
				
				if ($tops.is('.hover')) {
					// menu already open on another item, hard-cut to new dropdown
					timeout = window.setTimeout(function() {
						doOpen($li, false);
					}, 250);
					
				} else {
					// no menu currently showing, animate
					doOpen($li, true);
				}
			}
			
			// Does the actual open, with animation if desired
			function doOpen($li, animate) {
				$header.addClass('zippmenu-is-open');

				$tops.removeClass('hover');
				$tops.children('div').stop(true, true).hide();
				$li.addClass('hover');

				if (animate) {
					$li.children('div').delay(350).slideDown(350);
				} else {
					$li.children('div').delay(350).show();
				}
			}

			function showZippmenu() {

			}
			
			// Prepare to do a close; it might be cancelled later on
			function close() {
				window.clearTimeout(timeout);
				timeout = window.setTimeout(doClose, 500);
			}
			
			// Close a top-level item
			function doClose() {
				window.clearTimeout(timeout);

				$tops.removeClass('hover');
				$tops.children('div').fadeOut(350);
				setTimeout(function(){
					$header.removeClass('zippmenu-is-open');
				}, 350);
				itemClicked = {};
			}
			
			
			var itemClicked = {};
			// Hover over an A - open the popup
			$topAs.bind('click', function(e) {
				if($(window).width() > 765){
					if(itemClicked === $(this).html()) {
					} else if(itemClicked != $(this).html()) {
						e.preventDefault();
						open($(this).parent());
						itemClicked = $(this).html();
					}
				}
			});

			
			// Hover off an A - close the popup
			$topAs.bind('mouseout', close);
			
			// Hover over a DIV - cancel the opening or closing timer
			$tops.children('div').bind('mouseover', function() {
				window.clearTimeout(timeout);
				return false;
			});

			// Hover over another top level navigation item - cancel the opening or closing timer
			$topAs.bind('mouseover', function() {
				window.clearTimeout(timeout);
				return false;
			});
			
			// Hover off a DIV - close the popup
			$tops.children('div').bind('mouseout', close);
		});
	};
	
})(jQuery);