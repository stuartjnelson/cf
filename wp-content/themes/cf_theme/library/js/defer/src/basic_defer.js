// Defer Images
var imgDefer = document.getElementsByTagName('img'); for (var i=0; i<imgDefer.length; i++) { if(imgDefer[i].getAttribute('data-src')) { imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src')); } }

// Defer YouTube Videos
for(var vidDefer=document.getElementsByTagName("iframe"),i=0;i<vidDefer.length;i++)vidDefer[i].getAttribute("data-src")&&vidDefer[i].setAttribute("src",vidDefer[i].getAttribute("data-src"));

// Add tracking/anaylitic codes to the end of this script
// Some Google Script..
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-59613412-1', 'auto');
ga('send', 'pageview');