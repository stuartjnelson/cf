<?php
/***
* 		Template Name: Grid Page
***/

get_header(); ?>

<section class="section--fullWidth section--hero">
	
	<h1 class="section__title section__title--center"><?php the_title(); ?></h1>

	<div class="teamMembersContainer">

		<?php if ( have_rows('grid_item') ) : ?>
		
		  <?php while( have_rows('grid_item') ) : the_row(); ?>

		  	<article class="teamMemberContainer">
		  		<?php 
		  			if( get_sub_field('link_to_individual_bio_page') ) {
		  				$hover__title 	= 'teamMember__text--hover';
						$hover__bio 	= 'teamMember__bio--hover';
		  			} 
		  			else {
		  				$hover__title 	= null;
						$hover__bio 	= null;
		  			}
		  		?>	
		    	<div class="teamMember__text <?php // echo $hover__title; ?>">
		    		<?php //if (get_sub_field('picture_or_coloured_background') == 'coloured_background' ) : ?>
						<h3 class="teamMember__name teamMember__name--mobile">
							<?php echo get_sub_field('grid_item_title'); ?>
						</h3>
					<?php //endif; ?>
					<p class="teamMember__bio <?php //echo $hover__bio; ?>">
						<?php the_sub_field('copy'); ?>
					</p>
		    	</div>
				<?php if( get_sub_field('link_to_individual_bio_page') ) : ?>
					<a href="<?php the_sub_field('link_to_individual_bio_page'); ?>">
				<?php endif; ?>		
					<?php if (get_sub_field('picture_or_coloured_background') == 'coloured_background') {
						$coloredSquare = 'coloredSquare'; 
					} else {
						$coloredSquare = null;
					}
					 ?>
		    		<div class="teamMember__imageContainer <?php echo $coloredSquare; ?>" style="background-color:<?php the_sub_field('coloured_background');?>">
						<?php //if (get_sub_field('picture_or_coloured_background') == 'coloured_background' ) : ?>
							<h3 class="teamMember__name teamMember__name--gridItem">
								<?php echo get_sub_field('grid_item_title'); ?>
							</h3>
						<?php // endif; ?>

						<?php if (get_sub_field('profile_picture') && get_sub_field('picture_or_coloured_background') == 'picture' ) : ?>
							<?php 
								$image = get_sub_field('profile_picture');
								 
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								
								$size = 'large'; 
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
								
								//$new_size 	= '-250x250.jpg';
								//$new_url	= $url;
								//$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
							?>
							<img 
								class="teamMember__image" 
								src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
								data-src="<?php echo $url; ?>"
								alt="<?php echo $alt; ?>" 
								title="<?php echo $title; ?>" 
								width="<?php echo $width; ?>" 
								height="<?php echo $height; ?>"
							>
							<?php if( get_sub_field('link_to_individual_bio_page') ) : ?>
								<div class="imageHero__overlay"></div>
							<?php else : ?>	
								<div class="imageHero__overlay--noHover"></div>
							<?php endif; ?>	
						<?php else: ?>
						<?php endif; ?>
		    		</div>
				<?php if( get_field('link_to_individual_bio_page') ) : ?>		    		
		    		</a>
		    	<?php endif; ?>				
		    </article>	
		
		  <?php endwhile; ?>
		
		<?php endif; ?>

	</div>		
</section>

<?php get_footer(); ?> 	