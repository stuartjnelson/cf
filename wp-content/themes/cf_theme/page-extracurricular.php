<?php
/***
* 		Template Name: Extracurricular
***/

get_header(); ?>

<?php if (is_page('school-community-calendar') ): ?>
	<section class="section section--hero">
	
		<div class="wrapper--calendar wrapper--allCalendar">
			<h1 class="allCalendar--header"><?php the_title(); ?></h1>
			<?php echo do_shortcode('[ai1ec view="monthly"]'); ?>
		</div>
	</section>
<?php endif; ?>

<?php if (is_page('extra-curricular') ): ?>
	<section class="section section--hero section--extracurricular section--calendar">
		<div class="wrapper--calendar wrapper--extracurricular">
			<h1 class="extracurricular--header">
				<?php the_title(); ?>
			</h1>
			<div class="extracurricularEventsContainer">
				<?php //echo do_shortcode('[ai1ec view="posterboard" cat_name="monkton-farleigh-extra-curricular,atworth-extra-curricular"]'); ?>
			</div>	

<?php 

//global $ai1ec_registry;
//	$content         = '<div id="content2">';
//	$time            = $ai1ec_registry->get( 'date.system' );
//	// Get localized time
//	$timestamp       = $time->current_time();
//	// Set $limit to the specified categories/tags
//	$limit           = array(
//		//
//		// this is demo data - please use your own filters
//		//
//		'post_ids' => array ( 1041 ) 
//	);
//	$events_per_page = 20;
//	$paged           = 0;
//	$event_results   = $ai1ec_registry->get( 'model.search' )
//		->get_events_relative_to(
//			$timestamp,
//			$events_per_page,
//			$paged,
//			$limit
//		);
//	$dates = $ai1ec_registry->get(
//			'view.calendar.view.agenda',
//			$ai1ec_registry->get( 'http.request.parser' )
//		)->get_agenda_like_date_array( $event_results['events'] );
//	foreach ( $dates as $date ) {
//		foreach ( $date['events']['allday'] as $instance ) {
//			$post_title   = $instance->get( 'post' )->post_title;
//			$post_name    = $instance->get( 'post' )->post_name;
//			$post_content = $instance->get( 'post' )->post_content;
//			$instance_id  = $instance->get( 'instance_id' );
//			$content     .= '<div class="hp-event">
//				<a href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">'
//				.$post_title.'</a><br />'
//				.$post_content.
//				'<a class="read-more-link"  href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">read more</a>
//			</div>';
//		}
//		foreach ( $date['events']['notallday'] as $instance ) {
//				$post_title   = $instance->get( 'post' )->post_title;
//				$post_name    = $instance->get( 'post' )->post_name;
//				$post_content = $instance->get( 'post' )->post_content;
//				$instance_id  = $instance->get( 'instance_id' );
//				$content     .= '<div class="hp-event">
//					<a href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">'
//					.$post_title.'</a><br />'
//					.$post_content.
//					'<a class="read-more-link"  href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">read more</a>
//					</div>';
//		}
//	}
//	$content .= '</div>';
//	return $content;   

?>

<?php echo do_shortcode(['ai1ec2_display_events_func']); ?>
<?php //echo do_shortcode(['display_events2']‘); ?>

		</div>
	</section>
<?php endif ?>

<?php get_footer(); ?>