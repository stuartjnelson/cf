<?php if (get_field('use_slider') == 'slider' || get_field('use_slider') == 'image' || get_field('add_hero_image') == 'yes' ): ?>
		<?php $heroClass = null; ?>
  		<section class="section-fullWidth section--hero section--slider">
  			<?php 
				if (get_field('colour_of_page_titles') == 'black') {
					$color = 'slider__title--dark'; 
					$bgColor = 'pageSlider__image--dark';
				}
				else {
					$color = null; 
					$bgColor = null;
				}
			?>
	
			<!-- Slider -->
			<?php if (get_field('use_slider') == 'slider') : ?>
				<div class="pageSlider__slider">
					<?php get_template_part( 'partials/slider', 'hero' ); ?>
				</div>
	
			<!-- Single Image -->
			<?php elseif (get_field('use_slider') == 'image' || get_field('add_hero_image') == 'yes' ): ?>
				<div class="pageSlider__image pageSlider__image--overlay <?php echo $bgColor; ?>">
					<?php 
						$image = get_field('single_image');
						 
						$url = $image['url'];
						$title = $image['title'];
						$alt = $image['alt'];

						if(is_handheld()) {
							$size = 'hero--mobile'; // add image size
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
	
							$new_size 	= '-720x350.jpg';
							$new_url	= $url;
							$new_image	= preg_replace('/.jpg/', $new_size, $new_url);					
						}
						
						else {
							$size = 'hero'; // add image size
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
	
							$new_size 	= '-1440x350.jpg';
							$new_url	= $url;
							$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
						}	
					?>
					<img 
						class="single__image single__image--overlay" 
						src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
						data-src="<?php echo $url; ?>"
						alt="<?php echo $alt; ?>" 
						title="<?php echo $title; ?>" 
						width="<?php echo $width; ?>" 
						height="<?php echo $height; ?>"
					>
				</div>
			<?php endif; ?>
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="pageSlider__titlesContainer">
					<h1 class="pageSlider__title titles__pageTitle <?php echo $color; ?>"><?php the_title(); ?></h1>
					
					<?php if (is_single()): ?>
						<div class="pageSlider__title pageSlider__title--meta">
							<h4 class="titles__meta titles__meta--date <?php echo $color; ?>"><?php the_modified_date('j\<\s\u\p\>S\<\/\s\u\p\> F Y'); ?></h4>
							<h4 class="titles__meta <?php echo $color; ?>"><?php the_author(); ?></h4>
						</div>	
					<?php else: ?>
					<?php endif ?>
					
				</div>	
			<?php endwhile; endif; ?>
		</section>
	<?php else: $heroClass = 'section--hero'; ?>	
<?php endif; ?>	

<section class="section <?php echo $heroClass; ?>">
	<?php get_template_part('partials/page', 'breadcrumbs'); ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>		
			<div class="wpContentContainer">
				<?php if (get_field('use_slider') == 'hide' || get_field('add_hero_image') == 'no' ): ?>
					<h1>
						<?php the_title(); ?>
					</h1>
				<?php endif ?>
				<?php if ( get_field('main_content') ) : ?>
				  <?php echo get_field('main_content'); ?>
				<?php endif; ?>
				<?php if (is_singular('post')) : ?>
					<?php get_template_part('partials/social', 'sharingButtons'); ?>
				<?php endif ?>
			</div>
		<?php endwhile; endif; ?>
</section> 	