<?php
/***
* 		Template Name: Three Column Page
***/

get_header(); ?>

	<section class="section--hero section--wide">

	    <div class="columnContainer">

			<h1 class="section__title"><?php echo get_the_title(); ?></h1>

	    	<div class="column column__one">
	    		<?php if ( get_field('column_one') ) : ?>
	    			<?php echo get_field('column_one'); ?>
	    		<?php endif; ?>	
	    	</div>
	    	<div class="column column__two">
	    		<?php if ( get_field('column_two') ) : ?>
	    		  <?php echo get_field('column_two'); ?>
	    		<?php endif; ?>
	    	</div>
	    	<div class="column column__three">
	    		<?php if ( get_field('column_three') ) : ?>
	    		  <?php echo get_field('column_three'); ?>
	    		<?php endif; ?>
	    	</div>
	    </div>
	</section>

<?php get_footer(); ?> 	