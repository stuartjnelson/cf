<?php

// ------------------------------------------
//          Grunt Livereload
// ------------------------------------------
//if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
//  wp_register_script('livereload', 'http://localhost:80/livereload.js?snipver=1', null, false, true);
//  wp_enqueue_script('livereload');
//}

// ------------------------------------------
//          Cleaing up WP Head
// ------------------------------------------

function head_cleanup() {
    // category feeds
    // remove_action( 'wp_head', 'feed_links_extra', 3 );
    // post and comment feeds
    // remove_action( 'wp_head', 'feed_links', 2 );
    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );
    // windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );
    // index link
    remove_action( 'wp_head', 'index_rel_link' );
    // previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    // start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    // links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // WP version
    remove_action( 'wp_head', 'wp_generator' );
    // remove WP version from css
    add_filter( 'style_loader_src', 'joints_remove_wp_ver_css_js', 9999 );
    // remove Wp version from scripts
    add_filter( 'script_loader_src', 'joints_remove_wp_ver_css_js', 9999 );

} /* end head cleanup */

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

// remove WP version from RSS
function rss_version() { return ''; }

// remove WP version from scripts
function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}



// ------------------------------------------
//          Making Email Addreses Safe
// ------------------------------------------
function security_remove_emails($content) {
    $pattern = '/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/i';
    $fix = preg_replace_callback($pattern,"security_remove_emails_logic", $content);
    return $fix;
}

function security_remove_emails_logic($result) {
    return antispambot($result[1]);
}

add_filter( 'the_content', 'security_remove_emails', 20 );
add_filter( 'widget_text', 'security_remove_emails', 20 );




// ------------------------------------------
//          Adding Font AWESOME & Enque file
// ------------------------------------------
//add_action( 'wp_enqueue_scripts', 'load_font_awesome', 99 );
//
///* Enqueue Font Awesome Stylesheet from MaxCDN */
//function load_font_awesome() {
//    
//    wp_enqueue_style( 'font-awesome', 'http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css', null, '4.0.1' );
//}
//add_action( 'wp_enqueue_scripts', 'load_font_awesome' );  



function scripts_with_jquery()
{

    // Deregister the included library
    wp_deregister_script( 'jquery' );
     
    // Register the library again from Google's CDN
    //wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js', array(), null, false );
    //wp_enqueue_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri().'/library/js/jquery.min.2.1.1.js', array(), null, false );
    wp_enqueue_script( 'jquery' );


    // Fold scrips
    // Scripts that have to be loaded for the site to work
    wp_register_script( 'fold-scripts', get_template_directory_uri().'/library/js/fold.js', array('jquery'), null, true );
    wp_enqueue_script( 'fold-scripts' );



    //Scripts below added to defer.js
    //Scripst that have been added to defer. 

}
add_action( 'wp_enqueue_scripts', 'scripts_with_jquery' ); 

// Loading Google Map only on contact page
function load_map_script(){
    if ( is_page( 'contact' ) && (!is_mobile())) {
        wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA4s1vzbgDzY5pNn5mOQrK-4MS7Oojt92Q&sensor=false');
        wp_enqueue_script( 'google-maps' );
        
        wp_register_script('map-script', get_template_directory_uri() . '/library/js/map.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'map-script' );
    }
}
add_action( 'wp_print_scripts', 'load_map_script'); 


function load_css() 
{
    wp_enqueue_style( 'reset', get_template_directory_uri().'/library/css/reset.css' );
    wp_enqueue_style( 'custom-style', get_template_directory_uri().'/library/css/custom-style.css' );
    //wp_enqueue_style( 'custom-style', get_template_directory_uri().'/library/css/custom-style.min.css' );
}
add_action( 'wp_enqueue_scripts', 'load_css' ); 

// Remove CF7 CSS & Script
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


// Remove TablePress CSS & Script
add_action('wp_print_styles', 'mytheme_dequeue_css_from_plugins', 100);
function mytheme_dequeue_css_from_plugins()  {
    wp_dequeue_style( 'tablepress-default' ); 
    wp_deregister_script('tablepress-datatables'); 
}    

// ------------------------------------------
//          Registering Menu
// ------------------------------------------
//function register_theme_menu() {
//    register_nav_menu( array(
//        'primary', 'Main Menu',
//        'footer_menu', 'Footer Menu', 
//        )
//    );
//}
//add_action( 'init', 'register_theme_menu' );


register_nav_menus( array(
    'primary' => 'Main Menu',
    //'footer_menu' => 'Footer Menu', 
    'mobile_menu' => 'My Mobile Menu',
) );

add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );
function add_menu_parent_class( $items ) {
    
    $parents = array();
    foreach ( $items as $item ) {
        if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
            $parents[] = $item->menu_item_parent;
        }
    }
    
    foreach ( $items as $item ) {
        if ( in_array( $item->ID, $parents ) ) {
            $item->classes[] = 'menu-item--parent'; 
        }
    }
    
    return $items;    
}

//class My_Walker_Nav_Menu extends Walker_Nav_Menu {
//    
//    //function start_lvl(&$output, $depth) {
//    //    //$count = 1;
//    //    //$id = $count++;
//    //    $indent = str_repeat("\t", $depth);
//    //    $output .= "\n$indent<ul class=\"subMenu\">\n";
//    //}
//
//    function start_lvl( &$output, $depth = 0, $args = array() ) {
//        $title = get_field('parent_menu_item', 'options');
//        $copy  = get_field('sub_menu_paragraph', 'options');
//
//
//        $indent = str_repeat("\t", $depth);
//        $output .= "\n$indent<div class='subMenuContainer'>\n";
//        $output .= "<h3 class='subMenu__title'>";
//        $output .= $title;
//        $output .= "</h3>";
//        $output .= "<div class='subMenu__leftColumn'>\n";
//        $output .= "<div class='subMenu__copy'><p>";
//        $output .= $copy;
//        $output .= "</p></div>";                                            //Copy
//        $output .= "</div>";                                            //Left Column    
//        $output .= "\n$indent<ul class=\"subMenu__list\">\n";
//        $output .= "\n$indent<div class='subMenu__rightColumn'>\n";
//        $output .= "</div>";
//    }
//    function end_lvl( &$output, $depth = 0, $args = array() ) {
//        $indent = str_repeat("\t", $depth);
//        $output .= "</ul>\n$indent</div>\n";
//    }
//}

add_filter('wp_nav_menu_items','add_custom_in_menu', 10, 2);
function add_custom_in_menu( $items, $args ) {
    if( $args->theme_location == 'mobile_menu')  {
         
        $items .=  '<li>
                        <div class="headerSocial headerSocial--mobile">
                            <div class="headerSocial__item"><a href="#"><i class="headerSocial__item--icon fa fa-facebook"></i></a></div>
                            <div class="headerSocial__item"><a href="#"><i class="headerSocial__item--icon fa fa-google-plus"></i></a></div>
                        </div>
                    </li>';
  
    }
    return $items;
}

function get_id_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

//function depth_classes_wpse_130358($item_output, $item, $depth, $args) {
//  add_action(
//    'nav_menu_css_class',
//    function() use ($depth) {
//      $depth++;
//      $classes[] = "depth-{$depth}";
//      return $classes;
//    }
//  );
//  return $item_output;
//}
//add_filter('walker_nav_menu_start_el','depth_classes_wpse_130358',1,4);


// ------------------------------------------
//          Adding ACF Options Page
// ------------------------------------------
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}


// ------------------------------------------
//          Customising the admin menu 
//          Rename, remove & re-order the menu — http://code.tutsplus.com/articles/customizing-your-wordpress-admin--wp-24941
// ------------------------------------------

//function edit_admin_menus() {
//    global $menu;
//    global $submenu;
//
//    // Removing Appearance/menus
//    remove_menu_page('edit.php'); // Remove posts
//    remove_menu_page('edit-comments.php'); // Remove posts
//}
//add_action( 'admin_menu', 'edit_admin_menus' );

function edit_admin_menus() {
    global $menu;
     
    $menu[5][0] = 'Churchfields Blog'; // Change Posts to School Blog
}
add_action( 'admin_menu', 'edit_admin_menus' );



// ------------------------------------------
//          Adding Featured image support
// ------------------------------------------
function featured_image_theme_support() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'featured_image_theme_support' );



// ------------------------------------------
//          Adding New Post Image Size
// ------------------------------------------
// 300 square
//add_image_size( '300_square', 300, 300, array( 'center', 'center' ) );
// Logo
add_image_size( '100_square', 100, 100, array( 'center', 'center' ) );
add_image_size( '250_square', 250, 250, array( 'center', 'center' ) );
// BLog Post
add_image_size( 'blog_post_excerpt', 400, 300, array( 'center', 'center' ) );
add_image_size( 'header_image_thumb', 600, 450, array( 'center', 'center' ) );
add_image_size( 'blogFooterThumbail', 100, 70, array( 'center', 'center' ) );
//Staff

//Hero
add_image_size( 'hero--mobile', 720, 350, array( 'center', 'center' ) );
add_image_size( 'hero', 1440, 350, array( 'center', 'center' ) );


// ------------------------------------------
// Allow SVG Uploads
// ------------------------------------------
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



// ------------------------------------------
//          Automatically adding alt tags
// ------------------------------------------
function add_alt_tags($content)
{
        global $post;
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images))
        {
                foreach($images[1] as $index => $value)
                {
                        if(!preg_match('/alt=/', $value))
                        {
                                $new_img = str_replace('<img', '<img alt="'.$post->post_title.'"', $images[0][$index]);
                                $content = str_replace($images[0][$index], $new_img, $content);
                        }
                }
        }
        return $content;
}
add_filter('the_content', 'add_alt_tags', 99999);


// ------------------------------------------
//          Register Social Custom Post Type
// ------------------------------------------           
require_once(get_template_directory() . '/post-school_news.php');

// ------------------------------------------
//          Widgets
// ------------------------------------------      

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

    register_sidebar( array(
        'name'          => 'Calendar Widget',
        'id'            => 'calendar_widget',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );




function ai1ec2_display_events_func( $atts ) {
    global $ai1ec_registry;
    $content         = '<div id="content2">';
    $time            = $ai1ec_registry->get( 'date.system' );
    // Get localized time
    $timestamp       = $time->current_time();
    // Set $limit to the specified categories/tags
    $limit           = array(
        //
        // this is demo data - please use your own filters
        //
        'post_ids' => array ( 1041 ) 
    );
    $events_per_page = 20;
    $paged           = 0;
    $event_results   = $ai1ec_registry->get( 'model.search' )
        ->get_events_relative_to(
            $timestamp,
            $events_per_page,
            $paged,
            $limit
        );
    $dates = $ai1ec_registry->get(
            'view.calendar.view.agenda',
            $ai1ec_registry->get( 'http.request.parser' )
        )->get_agenda_like_date_array( $event_results['events'] );
    foreach ( $dates as $date ) {
        foreach ( $date['events']['allday'] as $instance ) {
            $post_title   = $instance->get( 'post' )->post_title;
            $post_name    = $instance->get( 'post' )->post_name;
            $post_content = $instance->get( 'post' )->post_content;
            $instance_id  = $instance->get( 'instance_id' );
            $content     .= '<div class="hp-event">
                <a href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">'
                .$post_title.'</a><br />'
                .$post_content.
                '<a class="read-more-link"  href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">read more</a>
            </div>';
        }
        foreach ( $date['events']['notallday'] as $instance ) {
                $post_title   = $instance->get( 'post' )->post_title;
                $post_name    = $instance->get( 'post' )->post_name;
                $post_content = $instance->get( 'post' )->post_content;
                $instance_id  = $instance->get( 'instance_id' );
                $content     .= '<div class="hp-event">
                    <a href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">'
                    .$post_title.'</a><br />'
                    .$post_content.
                    '<a class="read-more-link"  href="ai1ec_event/'.$post_name.'/?instance_id='.$instance_id.'">read more</a>
                    </div>';
        }
    }
    $content .= '</div>';
    return $content; 
}
add_shortcode( 'display_events2', 'ai1ec2_display_events_func' );
?>