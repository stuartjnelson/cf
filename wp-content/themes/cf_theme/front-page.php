<?php get_header(); ?>

<section class="section--fullwidth section--hero">
	<div class="imageHero">
		<?php 
			$image = get_field('left_school_image');
			 
			
			$url = $image['url'];
			$title = $image['title'];
			$alt = $image['alt'];
			
			$size = 'large'; 
			$thumb = $image['sizes'][ $size ];
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];
		
		?>
		<?php $posts = get_field('left_school_link');		
			if( $posts ): ?>
			    <?php foreach( $posts as $post): ?>
			        <?php setup_postdata($post); ?>
			            <a class="imageHero__link" href="<?php the_permalink(); ?>">
							<div class="imageHero__imageContainer">
								<h2 class="imageHero__schoolTitle imageHero__schoolTitle--top">Atworth Base</h2>
									<img  
										class="imageHero__image"
										src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
										data-src="<?php echo $url; ?>"
										alt="<?php echo $alt; ?>" 
										title="<?php echo $title; ?>" 
										width="<?php echo $width; ?>" 
										height="<?php echo $height; ?>"
									>
								<div class="imageHero__overlay"></div>
							</div>
						</a>
				<?php endforeach; ?>
		    <?php wp_reset_postdata(); ?>
		<?php endif; ?>

		<?php
			$image = get_field('right_school_image');
			 
			
			$url = $image['url'];
			$title = $image['title'];
			$alt = $image['alt'];
			
			$size = 'large'; 
			$thumb = $image['sizes'][ $size ];
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];
		
		?>
		<?php $posts = get_field('right_school_link');		
		if( $posts ): ?>
		    <?php foreach( $posts as $post): ?>
		        <?php setup_postdata($post); ?>
		            <a class="imageHero__link" href="<?php the_permalink(); ?>">
		            	<div class="imageHero__imageContainer">
							<h2 class="imageHero__schoolTitle">Monkton Farleigh Base</h2>
								<img  
									class="imageHero__image"
									src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
									data-src="<?php echo $url; ?>"
									alt="<?php echo $alt; ?>" 
									title="<?php echo $title; ?>" 
									width="<?php echo $width; ?>" 
									height="<?php echo $height; ?>"
								>
							<div class="imageHero__overlay"></div>
						</div>
					</a>
				<?php endforeach; ?>
		    <?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</div>
</section>

<section class="section">
	<div class="homepageHeader__container">
		<h2 class="homepageHeader">
			<?php echo get_field('homepage_tagline'); ?>
		</h2>
	</div>
	<article class="textContainer">
		<h3 class="textContainer__title">
			<?php if ( get_field('intro_title') ) : ?>
		 	 	<?php echo get_field('intro_title'); ?>
			<?php endif; ?>
		</h3>
		<p>
			<?php if ( get_field('intro_paragraph') ) : ?>
			  <?php echo get_field('intro_paragraph'); ?>
			<?php endif; ?>
		</p>
	</article>
</section>

<section class="section--fullWidth section--blogPosts">
	<h3 class="section__title section__title--homepage">Blog</h3>
	 
	 <?php 
        $args = array( 
          'post_type'       => 'post',
          'posts_per_page'  => 3,
          //'order'           => 'DEC'
        );
        $the_query = new WP_Query( $args );
	?>
		
	<?php 
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post(); 
	?>

	<article class="blogPost">
		<a href="<?php the_permalink();?>">
		<?php if (get_field('single_image')): ?>
			<?php 
				$image = get_field('single_image');
				 
				
				$url = $image['url'];
				$title = $image['title'];
				$alt = $image['alt'];
				
				$size = 'header_image_thumb'; 
				$thumb = $image['sizes'][ $size ];
				$width = $image['sizes'][ $size . '-width' ];
				$height = $image['sizes'][ $size . '-height' ];
				$new_size 	= '-600x450.jpg';
				$new_url	= $url;
				$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
			
			?>
			<img 
				class="blogPost__image" 
				src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
				data-src="<?php echo $new_image; ?>"
				alt="<?php echo $alt; ?>" 
				title="<?php echo $title; ?>" 
				width="<?php echo $width; ?>" 
				height="<?php echo $height; ?>"
			>
		<?php endif ?>

		<h4 class="blogPost__title">
			<?php the_title(); ?>
		</h4>
		</a>
		<p class="blogPost__text">		         	   		
			<?php	
		    	$content = get_field('main_content'); 
				$excerpt = mb_strimwidth($content, 0, 225, '...');
				echo wp_strip_all_tags($excerpt);
			?>
		</p>
		<a href="<?php the_permalink();?>" class="button blogPost__link">Read more</a>
	</article>

<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>