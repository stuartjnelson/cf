<?php
/***
* 		Template Name: Image Slider
***/
get_header(); ?>

	<?php if (get_field('header_image')): ?>
		<?php $heroClass = null; ?>
	  	<section class="section--fullWidth section--hero">
			<div class="pageSlider__image">
				<?php 
					$image = get_field('header_image');
		
					$url = $image['url'];
					$title = $image['title'];
					$alt = $image['alt'];
		
					if(is_handheld()) {
						$size = 'hero--mobile'; // add image size
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
			
						$new_size 	= '-720x350.jpg';
						$new_url	= $url;
						$new_image	= preg_replace('/.jpg/', $new_size, $new_url);					
					}
					
					else {
						$size = 'hero'; // add image size
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
			
						$new_size 	= '-1440x350.jpg';
						$new_url	= $url;
						$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
					}
				
				?>
				<img 
					class="post__image" 
					src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
					data-src="<?php echo $url; ?>"
					alt="<?php echo $alt; ?>" 
					title="<?php echo $title; ?>" 
					width="<?php echo $width; ?>" 
					height="<?php echo $height; ?>"
				>
			</div>
		</section>	
			
	<?php else : ?>	
		<?php $heroClass = 'section--hero'; ?>	
	<?php endif; ?>

	<section class="section slider__wrapper <?php echo $heroClass; ?>">
		<?php get_template_part('partials/page', 'breadcrumbs'); ?>
	
			<?php while ( have_posts() ) : the_post(); ?>
	
				<h2 class="single__title"><?php the_title(); ?></h2>
	
				<p class="pageSingle__content">
					<?php the_content(); ?>
				</p>
	
			<?php endwhile; ?>
	</section>
	
<?php get_footer(); ?> 	