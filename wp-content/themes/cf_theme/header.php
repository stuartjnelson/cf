<!DOCTYPE html>
<html> 
    <head> 
        
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title> 

        <!-- Favicon -->
        <link href="<?php echo get_template_directory_uri(); ?>/favicon.ico" rel="icon"/>
        <link href="<?php echo get_template_directory_uri(); ?>/favicon.png" rel="icon"/>
        
        <!-- CSS -->
        <link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" media="all" type="text/css" />

        <?php wp_head(); ?>
                                                                                                            
    </head> 

    <body> 

        <header class="header">
            <!-- Logo text or image -->

            <?php if (is_front_page()) : ?>
                <h1>
                    <a href="<?php echo get_site_url(); ?>">               
                        <img 
                            class="logo logo__image" 
                            src="<?php echo get_template_directory_uri(); ?>/library/img/churchfield_school_logo-100.png"
                            alt="Churchfield School Logo" 
                            title="Churchfield School Logo" 
                            width="100px" 
                            height="84px"
                        >
                    </a> 
                </h1>
            <?php else: ?>
                <a href="<?php echo get_site_url(); ?>">               
                    <img 
                        class="logo logo__image" 
                        src="<?php echo get_template_directory_uri(); ?>/library/img/churchfield_school_logo-100.png"
                        alt="Churchfield School Logo" 
                        title="Churchfield School Logo" 
                        width="100px" 
                        height="84px"
                    >
                </a>   
            <?php endif ?>

            <div class="burger mobileNavButton">
                <i class="mobileNavButton__icon fa fa-bars"></i>
            </div>
          
            <!-- Navigation -->
            <?php if ( !is_handheld() ) : ?>
                <nav class="mainMenu">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'depth' => 1 ) ); ?><!-- 'depth' => 1, -->
                </nav><!-- .main -->  

                <!--<div class="headerSocial">
                    <div class="headerSocial__item">
                        <a href="https://www.facebook.com/Churchfieldsvillageschool?fref=ts">
                            <i class="headerSocial__item--icon fa fa-facebook"></i>
                        </a>
                    </div>
                    <div class="headerSocial__item">
                        <a href="https://plus.google.com/103380798312453082732/about">
                            <i class="headerSocial__item--icon fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>-->
            <?php else: ?>
            <?php endif ?>
        </header>

        <?php //if ( is_handheld() ) : ?>
            <nav class="mobileMenu">
                <?php wp_nav_menu( array( 'theme_location' => 'mobile_menu', 'depth' => 1 ) ); ?>
                <?php // $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 4, ); wp_nav_menu( $defaults ); ?>
            </nav><!-- .main -->    
        <?php //else: ?>
        <?php //endif ?>

        <?php get_template_part('partials/menu', 'subMenu'); ?>
        <?php // get_template_part('partials/menu', 'subMenu--mobile'); ?>

        <main class="main">