<?php

// Register Custom Post Type
function school_news() {

	$labels = array(
		'name'                => _x( 'School News Posts', 'Post Type General Name', 'school_news' ),
		'singular_name'       => _x( 'School News Post', 'Post Type Singular Name', 'school_news' ),
		'menu_name'           => __( 'School Announcements', 'school_news' ),
		'name_admin_bar'      => __( 'School News Post', 'school_news' ),
		'parent_item_colon'   => __( 'School News Parent Post:', 'school_news' ),
		'all_items'           => __( 'All School News Posts', 'school_news' ),
		'add_new_item'        => __( 'Add New School News Post', 'school_news' ),
		'add_new'             => __( 'Add School News Post', 'school_news' ),
		'new_item'            => __( 'New School News Post', 'school_news' ),
		'edit_item'           => __( 'Edit School News Post', 'school_news' ),
		'update_item'         => __( 'Update School News Post', 'school_news' ),
		'view_item'           => __( 'View School News Post', 'school_news' ),
		'search_items'        => __( 'Search School News Post', 'school_news' ),
		'not_found'           => __( 'School News Post Not found', 'school_news' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'school_news' ),
	);
	$args = array(
		'label'               => __( 'School News Post', 'school_news' ),
		'description'         => __( 'School News Post', 'school_news' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'revisions', 'page-attributes', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'school_news', $args );

}
add_action( 'init', 'school_news', 0 );

?>