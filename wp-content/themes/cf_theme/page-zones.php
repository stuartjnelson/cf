<?php
/***
* 		Template Name: Zones Page
***/

get_header(); ?>

	<?php //get_template_part( 'pages/page', 'standard' ); ?>

	<section class="section--wide">
		
		<section class="section--hero">
			<h1 class="zonePageTitle"><?php the_title(); ?></h1>
		</section>

	    <div class="columnContainer">

			<?php $counter = 0; ?>
	    	<div class="column column__one">
	    		<?php if ( have_rows('column_one_zones') ) : ?>
	    		
	    		  <?php while( have_rows('column_one_zones') ) : the_row(); ?>
					
					<?php $counter++; ?>
	    		  	<article class="article zoneContainer articleRow--<?php echo $counter; ?> articleRow--<?php echo $counter; ?>--one" target="<?php echo $counter; ?>">
						<?php 
							$title = get_the_title();
							$slug = sanitize_title($title);
							$target = null;
	
 							$external__link = get_sub_field('external_zone_link');
	    		    	 	$internal__link = get_sub_field('internal_zone_link');
	
	    		    	 	if(get_sub_field('which_link') == 'external') {
								$link = $external__link;
								$target = '_blank';
	    		    	 	}
	    		    	 	elseif (get_sub_field('which_link') == 'internal') {
								//$link = $internal__link;


								if (strpos($internal__link,'?attachment_id=') !== false) {
									$url = $internal__link;
									$parts = Explode('/', $url);
									$id = $parts[count($parts) - 1];

									$data = $id;    
									$final_id = substr($data, strpos($data, "=") + 1);    
								
									$link = wp_get_attachment_url( $final_id );
								}
								else {
									$link = $internal__link;
								}	
	    		    	 	}
	    		    	 	else {
	    		    	 		$link = null;
	    		    	 	}
						?>
	
	    		  		<a href="<?php echo $link; ?>" alt="<?php echo $slug; ?>">
	    		    		<h3 class="zone__title">
	    		    			<?php the_sub_field('zone_title'); ?>
	    		    		</h3>

	    		    		<?php if (get_sub_field('zone_picture')): ?>
		
	    		    			<?php 
	    		    				$image = get_sub_field('zone_picture');
	    		    				 
	    		    				$url = $image['url'];
									$title = $image['title'];
									$alt = $slug;

									$size = 'medium'; 
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
									
									$new_size 	= '-300x200.jpg';
									$new_url	= $url;
									$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
	    		    			?>
	    		    			
	    		    			<img 
	    		    				class="zone__image" 
	    		    				src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
	    		    				data-src="<?php echo $new_image; ?>"
	    		    				alt="<?php echo $alt; ?>" 
	    		    				title="<?php echo $title; ?>" 
	    		    				width="<?php echo $width; ?>" 
	    		    				height="<?php echo $height; ?>"
	    		    			>
	    		    		<?php endif ?>	
	    		    	</a>	
	
	    		    	<div class="zone__copy">
	    		    		<?php the_sub_field('zone_copy'); ?>
	    		    	</div>

	    		    	<a href="<?php echo $link; ?>" alt="<?php echo $slug ?>" class="button zone__button" target="<?php echo $target; ?>">
	    		    		<?php //the_sub_field('zone_title') ?>
	    		    		<?php if ( get_field('button_text') ) : ?>
	    		    		  	<?php echo get_field('button_text'); ?>
							<?php else: ?>	
								Read More
	    		    		<?php endif; ?>
	    		    	</a>
	    		    </article>	
	    		
	    		  <?php endwhile; ?>
	    		
	    		<?php endif; ?>
	    	</div>

	    	<div class="column column__two">
	    		<?php $counter = 0; ?>
	    		<?php if ( have_rows('column_two_zones') ) : ?>
	    		
	    		  <?php while( have_rows('column_two_zones') ) : the_row(); ?>

	    		  	<?php $counter++; ?>
	    		  	<article class="article zoneContainer articleRow--<?php echo $counter; ?> articleRow--<?php echo $counter; ?>--two" target="<?php echo $counter; ?>">
						<?php 
							$title = get_the_title();
							$slug = sanitize_title($title);

							$target = null;
	
 							$external__link = get_sub_field('external_zone_link');
	    		    	 	$internal__link = get_sub_field('internal_zone_link');
	
	    		    	 	if(get_sub_field('would_you_like_to_add_a_link_for_this_zone') == 'external') {
								$link = $external__link;
								$target = '_blank';
	    		    	 	}
	    		    	 	elseif (get_sub_field('would_you_like_to_add_a_link_for_this_zone') == 'internal') {
								$link = $internal__link;
	    		    	 	}
	    		    	 	else {
	    		    	 		$link = null;
	    		    	 	}
						?>
	
	    		  		<a href="<?php echo $link; ?>" alt="<?php echo $slug; ?>">
	    		    		<h3 class="zone__title">
	    		    			<?php the_sub_field('zone_title'); ?>
	    		    		</h3>

	    		    		<?php if (get_sub_field('zone_picture')): ?>
		
	    		    			<?php 
	    		    				$image = get_sub_field('zone_picture');
	    		    				 
	    		    				
	    		    				$url = $image['url'];
	    		    				$title = $image['title'];
	    		    				$alt = $slug;
	    		    				
	    		    				$size = 'medium'; 
	    		    				$thumb = $image['sizes'][ $size ];
	    		    				$width = $image['sizes'][ $size . '-width' ];
	    		    				$height = $image['sizes'][ $size . '-height' ];
	    		    			
	    		    				$new_size 	= '-300x200.jpg';
	    		    				$new_url	= $url;
	    		    				$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
	    		    			?>
	    		    			
	    		    			<img 
	    		    				class="zone__image" 
	    		    				src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
	    		    				data-src="<?php echo $new_image; ?>"
	    		    				alt="<?php echo $alt; ?>" 
	    		    				title="<?php echo $title; ?>" 
	    		    				width="<?php echo $width; ?>" 
	    		    				height="<?php echo $height; ?>"
	    		    			>
	    		    		<?php endif ?>	
	    		    	</a>	
	
	    		    	<div class="zone__copy">
	    		    		<?php the_sub_field('zone_copy'); ?>
	    		    	</div>

	    		    	<a href="<?php echo $link; ?>" alt="<?php echo $slug ?>" class="button zone__button" target="<?php echo $target; ?>">
	    		    		<?php if ( get_field('button_text') ) : ?>
	    		    		  	<?php echo get_field('button_text'); ?>
							<?php else: ?>	
								Read More
	    		    		<?php endif; ?>
	    		    	</a>
	    		    </article>	
	    		
	    		  <?php endwhile; ?>
	    		
	    		<?php endif; ?>
	    	</div>

	    	<div class="column column__three">
	    		<?php $counter = 0; ?>
	    		<?php if ( have_rows('column_three_zones') ) : ?>
	    		
	    		  <?php while( have_rows('column_three_zones') ) : the_row(); ?>

	    		  	<?php $counter++; ?>
	    		  	<article class="article zoneContainer articleRow--<?php echo $counter; ?> articleRow--<?php echo $counter; ?>--three" target="<?php echo $counter; ?>">
						<?php 
							$title = get_the_title();
							$slug = sanitize_title($title);
							$target = null;
	
 							$external__link = get_sub_field('external_zone_link');
	    		    	 	$internal__link = get_sub_field('internal_zone_link');
	
	    		    	 	if(get_sub_field('would_you_like_to_add_a_link_for_this_zone') == 'external') {
								$link = $external__link;
								$target = '_blank';
	    		    	 	}
	    		    	 	elseif (get_sub_field('would_you_like_to_add_a_link_for_this_zone') == 'internal') {
								$link = $internal__link;
	    		    	 	}
	    		    	 	else {
	    		    	 		$link = null;
	    		    	 	}
						?>
	
	    		  		<a href="<?php echo $link; ?>" alt="<?php echo $slug; ?>">
	    		    		<h3 class="zone__title">
	    		    			<?php the_sub_field('zone_title'); ?>
	    		    		</h3>

	    		    		<?php if (get_sub_field('zone_picture')): ?>
		
	    		    			<?php 
	    		    				$image = get_sub_field('zone_picture');

	    		    				$url = $image['url'];
									$title = $image['title'];
									$alt = $slug;

									$size = 'medium'; 
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];

									$new_size 	= '-300x200.jpg';
									$new_url	= $url;
									$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
	    		    			?>
	    		    			
	    		    			<img 
	    		    				class="zone__image" 
	    		    				src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
	    		    				data-src="<?php echo $new_image; ?>"
	    		    				alt="<?php echo $alt; ?>" 
	    		    				title="<?php echo $title; ?>" 
	    		    				width="<?php echo $width; ?>" 
	    		    				height="<?php echo $height; ?>"
	    		    			>
	    		    		<?php endif ?>	
	    		    	</a>	
	
	    		    	<div class="zone__copy">
	    		    		<?php the_sub_field('zone_copy'); ?>
	    		    	</div>

	    		    	<a href="<?php echo $link; ?>" alt="<?php echo $slug ?>" class="button zone__button" target="<?php echo $target; ?>">
	    		    		<?php if ( get_field('button_text') ) : ?>
	    		    		  	<?php echo get_field('button_text'); ?>
							<?php else: ?>	
								Read More
	    		    		<?php endif; ?>
	    		    	</a>
	    		    </article>	
	    		
	    		  <?php endwhile; ?>
	    		
	    		<?php endif; ?>
	    	</div>
	    </div>
	</section>

<?php get_footer(); ?> 	