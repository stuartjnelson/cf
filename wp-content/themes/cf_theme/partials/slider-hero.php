<div id="glide" class="glide">

	<div class="glide__arrows">
		<span class="glide__arrow prev" data-glide-dir="<"><i class="fa fa-2x fa-arrow-left"></i></span>
		<span class="glide__arrow next" data-glide-dir=">"><i class="fa fa-2x fa-arrow-right"></i></span>
	</div>
	
	<ul class="glide__wrapper">
		<?php if( have_rows('image_slider') ): ?>
	
    		<?php while ( have_rows('image_slider') ) : the_row(); ?>
	
				<li class="glide__slide">

					<?php 

						$image = get_sub_field('slider_image');
						
						if( !empty($image) ): 
						
						// vars
						$url = $image['url'];
						$title = $image['title'];
						$alt = $image['alt'];
						
						// thumbnail
						$size = 'hero'; // add image size
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];

						$new_size 	= '-1440x350.jpg';
						$new_url	= $url;
						$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
					?>

					<img 
						class="glide__slide__img" 
						src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
						data-src="<?php echo $url; ?>"
						alt="<?php echo $alt; ?>" 
						title="<?php echo $title; ?>" 
						width="<?php echo $width; ?>" 
						height="<?php echo $height; ?>"
					>
					<?php endif; ?>
				</li>
	
   		 	<?php endwhile; ?>
	
		<?php endif; ?>
	</ul>

	<ul class="glide__bullets"></div>

</div>