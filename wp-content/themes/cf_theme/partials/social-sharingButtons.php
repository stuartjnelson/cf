<div class="socialShareButtons">
  <div class="socialShareButtons__item">
    <span class="fa fa-facebook" data-social="fb"></span>
  </div>
  <div class="socialShareButtons__item">
    <span class="fa fa-twitter" data-social="tw"></span>
  </div>
  <div class="socialShareButtons__item">
        <span class="fa fa-google-plus" data-social="gp"></span>
  </div>
  <?php 
  	$title = get_the_title();
  	$link = get_the_permalink();

  	$safeHttp 		= 'http%3A';
	$permalink		= $link;
	$safePermalink	= preg_replace('/http:/', $safeHttp, $permalink);
  ?>
  <a href="mailto:?&subject=<?php echo $title; ?>&body=Check%20out%20this%20great%20article%20from%20<?php echo $safePermalink; ?>" title="<?php echo $title ?>">
    <div class="socialShareButtons__item">
        <span class="fa fa-envelope"></span>
    </div>
  </a>
</div>