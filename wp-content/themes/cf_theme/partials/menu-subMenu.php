<!-- About Sub Menu -->
<div class="subMenuContainer subMenuContainer--about">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <h3 class="subMenu__title"><?php $id1 = get_id_by_slug('about-churchfields'); echo get_the_title($id1); ?></h3>
        <div class="subMenu__leftColumn">
            <div class="subMenu__copy">
                <p><?php the_field('sub_menu_paragraph', $id1); ?></p>
            </div>    
        </div>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 195, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- Meet The Team Sub Menu -->
<div class="subMenuContainer subMenuContainer--meet">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <h3 class="subMenu__title"><?php $id5 = get_id_by_slug('meet-the-team'); echo get_the_title($id5); ?></h3>
        <div class="subMenu__leftColumn">
            <div class="subMenu__copy">
                <p><?php the_field('sub_menu_paragraph', $id5); ?></p>
            </div>    
        </div>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 888, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- How Sub Menu -->
<div class="subMenuContainer subMenuContainer--how">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <h3 class="subMenu__title"><?php $id2 = get_id_by_slug('how-were-doing'); echo get_the_title($id2); ?></h3>
        <div class="subMenu__leftColumn">
            <div class="subMenu__copy">
                <p><?php the_field('sub_menu_paragraph', $id2); ?></p>
            </div>    
        </div>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 196, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- Learn Sub Menu -->
<div class="subMenuContainer subMenuContainer--learn">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <h3 class="subMenu__title"><?php $id3 = get_id_by_slug('learning'); echo get_the_title($id3); ?></h3>
        <div class="subMenu__leftColumn">
            <div class="subMenu__copy">
                <p><?php the_field('sub_menu_paragraph', $id3); ?></p>
            </div>    
        </div>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 197, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- News Sub Menu -->
<div class="subMenuContainer subMenuContainer--news">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <h3 class="subMenu__title"><?php $id4 = get_id_by_slug('news'); echo get_the_title($id4); ?></h3>
        <div class="subMenu__leftColumn">
            <div class="subMenu__copy">
                <p><?php the_field('sub_menu_paragraph', $id4); ?></p>
            </div>    
        </div>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 198, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>