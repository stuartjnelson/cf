<!-- About Sub Menu -->
<div class="subMenuContainer subMenuContainer__mobiel subMenuContainer__mobiel--about">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 195, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- Meet The Team Sub Menu -->
<div class="subMenuContainer subMenuContainer__mobiel subMenuContainer__mobiel--meet">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 888, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- How Sub Menu -->
<div class="subMenuContainer subMenuContainer__mobiel subMenuContainer__mobiel--how">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 196, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- Learn Sub Menu -->
<div class="subMenuContainer subMenuContainer__mobiel subMenuContainer__mobiel--learn">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>    
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 197, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>


<!-- News Sub Menu -->
<div class="subMenuContainer subMenuContainer__mobiel subMenuContainer__mobiel--news">
    <div class="subMenuContainer__internalContainer">
        <span class="mobileMenu__closeButton">
            <i class="fa fa-arrow-left"></i>
        </span>    
        <nav class="subMenu__rightColumn">
            <!-- <ul class="subMenu__list"></ul> -->
            <?php $defaults = array( 'theme_location' => 'main_menu', 'level' => 2, 'child_of' => 198, ); wp_nav_menu( $defaults ); ?>
        </nav>  
    </div>                  
</div>