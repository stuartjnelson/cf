<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
	<?php if (is_singular('post')): ?>
			<a href="<?php echo get_page_link(2); ?>">Churchfields Blog</a>
		</li>
		<li class="breadcrumbs__item">
			<i class="fa fa-chevron-right"></i>
		</li>
		<li class="breadcrumbs__item">
			<?php the_title(); ?>
		</li>
	<?php elseif (is_singular('ai1ec_event')) : ?>	
		<a href="<?php echo get_page_link(120); ?>">School Community Calendar</a>
		</li>
		<li class="breadcrumbs__item">
			<i class="fa fa-chevron-right"></i>
		</li>
		<li class="breadcrumbs__item">
			<?php the_title(); ?>
		</li>
	<?php elseif (is_singular('school_news')) : ?>		
		<a href="<?php echo get_page_link(215); ?>">School Announcements</a>
		</li>
		<li class="breadcrumbs__item">
			<i class="fa fa-chevron-right"></i>
		</li>
		<li class="breadcrumbs__item">
			<?php the_title(); ?>
		</li>
	<?php else: ?>
		<!-- <a href="<?php echo get_page_link(120); ?>">Parent Page</a> -->
	<?php endif; ?>
</ul>