<div id="Glide" class="glide header__glide">

	<!-- <div class="glide__arrows"> -->
	<!-- 	<span class="glide__arrow prev" data-glide-dir="<"><i class="fa fa-2x fa-arrow-left"></i></span> -->
	<!-- 	<span class="glide__arrow next" data-glide-dir=">"><i class="fa fa-2x fa-arrow-right"></i></span> -->
	<!-- </div> -->
	
	<ul class="glide__wrapper">
		<?php if( have_rows('testimonial_slider', 'option') ): ?>
	
    		<?php while ( have_rows('testimonial_slider', 'option') ) : the_row(); ?>
	
				<li class="glide__slide header__glide__slide">
					<div class="glide__slide__message">
						<h4><?php the_sub_field('testimonial_message', 'option'); ?></h4>
					</div>
					<div class="glide__slide__author">
						<?php the_sub_field('testimonial_author', 'option'); ?>
					</div>					
				</li>
	
   		 	<?php endwhile; ?>
	
		<?php endif; ?>
	</ul>

</div>