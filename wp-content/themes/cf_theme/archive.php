<?php get_header(); ?>

  	<section class="section--fullWidthPadding section--hero">
	
		<?php get_template_part('partials/page', 'breadcrumbs'); ?>	
		
		<h1 class="section__title section__title--blog">
			<?php if (is_page( 2 )): ?>
				<?php get_the_title(2); ?>
			<?php elseif (is_category()) : ?>
				<?php single_cat_title(); ?>
			<?php elseif(is_month()) : ?>	
				<?php the_time('F Y'); ?>
			<?php else : ?>	
				School Blog
			<?php endif; ?>
		</h1>

		<div class="blogArticlesContainer">
			<?php 
				if ( have_posts() ) : 
				while ( have_posts() ) : the_post(); 
			?>
			
				<article class="blogPost blogPost--2column">
					<div class="post__imageWrapper">
					<a href="<?php the_permalink();?>">
						<?php 
							$image = get_field('single_image');
							 
							
							$url = $image['url'];
							$title = $image['title'];
							$alt = $image['alt'];
							
							$size = 'header_image_thumb'; 
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
							$new_size 	= '-600x450.jpg';
							$new_url	= $url;
							$new_image	= preg_replace('/.jpg/', $new_size, $new_url);
						
						?>
						<img 
							class="blogPost__image" 
							src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
							data-src="<?php echo $new_image; ?>"
							alt="<?php echo $alt; ?>" 
							title="<?php echo $title; ?>" 
							width="<?php echo $width; ?>" 
							height="<?php echo $height; ?>"
						>
					</a>
					</div>
				
					<h3 class="blogPost__title"><?php the_title(); ?></h3>
					
					<h4 class="blogPost__date"><?php echo get_the_date(); ?></h4>
				
					<p class="blogPost__text">		         	   		
						<?php	
					    	$content = get_the_content(); 
							$excerpt = mb_strimwidth($content, 0, 225, '...');
							echo wp_strip_all_tags($excerpt);
						?></p>
				
					<a href="<?php the_permalink();?>" class="button blogPost__button">Read more</a>

				</article>
		<?php endwhile; endif; ?>
	</div>
	
	<aside class="sidebar">
	
		<h3 class="sidebar__heading">Categories</h3>
		<ul class="sidebar__uList">
			<?php 
			    $args = array(
				'orderby'            => 'name',
				'order'              => 'ASC',
				'style'              => 'list',
				'hide_empty'         => 1,
				'use_desc_for_title' => 1,
				'hierarchical'       => 1,
				'title_li'           => '',
				'echo'               => 1,
				'depth'				 => -1,
				'taxonomy'           => 'category',
			    );
			    wp_list_categories( $args ); 
			?>
		</ul>
 
		<h3 class="sidebar__heading">Archives</h3>
			<ul class="sidebar__uList">
				<?php $args = array(
						'type'            => 'monthly',
						'format'          => 'html',
						'echo'            => 1,
						'order'           => 'DESC'
					);
				wp_get_archives( $args ); ?>
			</ul>
		
		<!-- <h3 class="sidebar__heading">Events</h3>
			<ul class="sidebar__uList">
				<a href="#">
					<li class="sidebar__list">Example event</li>
				</a>
			</ul>
 -->
		<?php //if ( is_active_sidebar( 'calendar_widget' ) ) : ?>
			<!-- <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
				<?php // dynamic_sidebar( 'calendar_widget' ); ?>
			</div> -->
		<?php //endif; ?>

	</aside>
	</section>	<!-- #content-->	

<?php get_footer(); ?> 