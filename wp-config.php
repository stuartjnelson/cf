<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cf_offline');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{8HaC$(@ON`@z^hDW3As5heLu]FQ|XSQf3|:1dEn,49I@hx[!Hb_uKMNr@ ~ZC2r');
define('SECURE_AUTH_KEY',  '>l3F<{-^OffY/HWP|]Yj++a%pg6^Xr&^fvU;3#ht7lE1zmDuaQ3vMsPkoxVr-$^i');
define('LOGGED_IN_KEY',    '$REH|f6oOR@EC.)q|c;L>t>9yXaiuxx3riU~c#FLNZ-3[W{z!M3{Aa(xN0A.C?MJ');
define('NONCE_KEY',        'b^}WbD5MJ4B(0]uYm8iYr|;/Yqa?hFel6wc+5yn<?F+}`]_rthz+6=Md`j87|~1i');
define('AUTH_SALT',        '2m+EA*~zIodPr(pq6`]?7YYKL;xc<_qIJ.&M@9ey6!,]BjnJ.*5Ds_sD^Wq&m}hZ');
define('SECURE_AUTH_SALT', 'GN)QP<omafzKRZd-M` l>>jaGl%6OH36//1%qT&AtA{=9fIYTm;GE9<[,R6rJ;4v');
define('LOGGED_IN_SALT',   '}83riQ7vS+tT<+VSpg.MW$8{O~S6R`jFFV#qV-4zIQ>Iq:9<&B$apX~Km.}sO/:)');
define('NONCE_SALT',       'jtgWr?mm2FAwO7>rMnOCf(h?O,Zdd L|{:Cm/j >IkwMRmQ(x3/GZcB{ngfhdQUu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sjdsd09388_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
