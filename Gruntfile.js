module.exports = function (grunt) {

	var pathCSS 		= 'library/css/custom-style.css';
	var pathSCSS 		= 'library/scss/custom-style.scss';
	var pathDefer_js	= 'library/js/defer/defer.js';

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		// Setup
		//copy: {
		//  main: {
		//    files: [
		//      // includes files within path and its sub-directories
		//      {expand: true, src: ['theme_files/**'], dest: '../'},
		//    ],
		//  },
		//},	
//
		//clean: ['theme_files'],	

		// Standard 
		sass: {
			dist: {
				options: {
					style: 'nested',
				},
			//dist: {									//whole dir
			//	files: [{
			//		expand: true,
			//		cwd: 'library/scss',
			//		src: ['*.scss'],
			//		dest: 'library/css',
			//		ext: '.css'
			//	}]
			//}		
				files: {
					'library/css/custom-style.css': 'library/scss/custom-style.scss'
				}
			},

			deploy: {
				options: {
					style: 'nested',
				},
			
				files: {
					'library/css/custom-style.css': 'library/scss/custom-style.scss'
				}	
			}		
		},

		autoprefixer: {
			single_file: {
				options: {
			  		browsers: ['last 2 versions'],
			  		map: {
		        		inline: false
		    		}	
			  	},
			  		src: pathCSS,
			  		dest: pathCSS
			},		
			// prefix all files
			//multiple_files: [{
			//  expand: true,
			//  flatten: true,
			//  src: 'src/css/*.css', // -> src/css/file1.css, src/css/file2.css
			//  dest: 'dest/css/' // -> dest/css/file1.css, dest/css/file2.css
			//}],
		},
			
		concat: {
			options: {
				seperator: ';',
				banner: '/* Northern Badger Concat */\n'
			},
			target: {
				src: 'library/js/defer/src/*.js',
				dest: pathDefer_js
			}
		},

		uglify: {
			options: {
				mangle: true,
				compress: true,
				sourceMap: true,
        		sourceMapName: 'library/js/uglify-sourcemap.map'
			},
			target: {
			 	//src: pathDefer_js,      
 				//dest: 'library/js/min/defer.min.js'
 				files: {
 					'library/js/min/defer.min.js': [pathDefer_js],
 					'library/js/min/fold.min.js': ['library/js/fold.js'],
 				}	
			}
		},	

		// Pre deply
 		//imagemin: {                          		// Task
 		//  	squash: {                          		// Target
 		//  	  	options: {                       	// Target options
 		//  	  	  	optimizationLevel: 4,
 		//  	  	  	svgoPlugins: [{ removeViewBox: false }],
 		//  	  	},                        
 		//  	  	files: [{
 		//  	  	  	expand: true,                  	// Enable dynamic expansion
 		//  	  	  	cwd: 'library/img/',            // Src matches are relative to this path
 		//  	  	  	src: ['**/*.{png,jpg,gif}'],   	// Actual patterns to match
 		//  	  	  	dest: 'library/optim/'  			// Destination path prefix
 		//  	  	}]
 		//  	}
 		//},


 		//imageoptim: {
		//  	allImg: {
		//  	  	options: {
		//  	  	  jpegMini: false,
		//  	  	  imageAlpha: true,
		//  	  	  quitAfter: true
		//  	  	},
		//  	  	src: ['library/img']
		//  	}
		//},

		exec: {
    	  get_grunt_sitemap: {
    	    command: 'curl --silent --output sitemap.json http://relent-wp/?show_sitemap'	//chnage url for local url dynamic generate data for gruntfile.js — https://github.com/dylang/grunt-prompt
    	  }
    	},
    	
    	uncss: {
    	  dist: {
    	    options: {
    	      ignore       : [/expanded/,/js/,/wp-/,/align/,/admin-bar/, /lib/,/library/, '.swing'],
    	      stylesheets  : [pathCSS],
    	      ignoreSheets : [/fonts.googleapis/],
    	      urls         : [], //Overwritten in load_sitemap_and_uncss task
    	    },
    	    files: {
    	      'style.css': ['**/*.php']
    	    }
    	  }
    	},  

    	// Running tasks
		notify_hooks: {
		    options: {
		      	enabled: true,
		      	success: false, // whether successful grunt executions should be notified automatically
		      	duration: 3 // the duration of notification in seconds, for `notify-send only
		    }
		},

		watch: {
			all: {
				files: ['library/scss/**/*.scss', 'library/js/defer/src/*', 'library/js/fold.js', '**/*.php'],	//Specify dir
				//files: ['library/**/*'],								//all files inside dir
				//files: ['!lib/dontwatch.js']							//!before/path means excluded
				tasks: ['sass', 'autoprefixer', 'concat', 'uglify', 'notify_hooks'],
				options: { livereload: true }
			}	
			
		}  
	});

	// Setup tasks
	//grunt.loadNpmTasks('grunt-contrib-copy');
	//grunt.loadNpmTasks('grunt-contrib-clean');

	// Standard tasks
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Deploy tasks
	grunt.loadNpmTasks('grunt-uncss');
	grunt.loadNpmTasks('grunt-exec');
	//grunt.loadNpmTasks('grunt-imageoptim');
	//grunt.loadNpmTasks('grunt-contrib-imagemin');

	// Running tasks
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-contrib-watch');		//Must be 2nd to last
	//grunt.loadNpmTasks('grunt-newer');				//Must be last


	grunt.registerTask('setup', ['copy', 'clean']);
	grunt.registerTask('default', ['sass', 'autoprefixer', 'concat', 'uglify', 'notify_hooks']);
	//grunt.registerTask('img', ['newer:imageoptim:allImg']);
	//grunt.registerTask('deploy', ['exec:get_grunt_sitemap','load_sitemap_json','uncss:dist','sass:dist']);		//relies on uncss being 0.1.1
	grunt.registerTask('deploy', ['sass:deploy','autoprefixer','concat', 'uglify', 'notify_hooks']);		//relies on uncss being 0.1.1
	//grunt.registerTask('setup', ['files', 'clean']);


	//grunt.registerTask('load_sitemap_json', function() {
    //	var sitemap_urls = grunt.file.readJSON('./sitemap.json');
    //	grunt.config.set('uncss.dist.options.urls', sitemap_urls); 
    //	//grunt.config.set('uncss.dist.files', {'style.css': [sitemap_urls]});	//fix for Phantom JS problem but cant get it working...
  	//});

  	//grunt.registerTask('files', function () {
	//	//grunt.file.write(“file.php”, “Writing some text”);
	//	grunt.file.mkdir('library/js/defer');
	//	grunt.file.mkdir('library/js/defer/src');
	//	grunt.file.copy('lib/js/defer/src/basic_defer.js', 'library/js/defer/src/basic_defer.js');
	//	grunt.file.copy('lib/js/fold.js', 'library/js/fold.js');
	//	//grunt.file.delete(“newFile.php”);		//grunt-contrib-clean to delete dir
	//});

};